// --------------------
//! @file
//! Copyright(c) All Rights Reserved
//!
//! Summary: Blink the arduino led

// cppcheck-suppress missingInclude
#include <Arduino.h>

// --------------------
//! Arduino setup
// cppcheck-suppress unusedFunction
void setup()
{
    // initialize the digital pin as an output.
    // Pin 13 has an LED connected on most Arduino boards:
    pinMode(13, OUTPUT);
}

//! delay about 50ms
#define DELAYCOUNT 50

// --------------------
//! Arduino main loop
// blink the LED rapidly for about a second
// turn it off for a full second
// and repeat
// cppcheck-suppress unusedFunction
void loop()
{
    // set the pin high which turns on the LED and then wait a bit
    digitalWrite(13, HIGH);
    delay(DELAYCOUNT);

    // do a turn on/turn off cycle 10 times
    // each cycle takes about 50 + 50 = 100ms
    for (int i = 0; i < 10; ++i) {
        // set the pin low which turns off the LED and then wait a bit
        digitalWrite(13, LOW);
        delay(DELAYCOUNT);

        // turn on the LED and wait
        digitalWrite(13, HIGH);
        delay(DELAYCOUNT);
    }

    // ensure the LED off
    digitalWrite(13, LOW);

    // wait for a second
    delay(1000);
}
