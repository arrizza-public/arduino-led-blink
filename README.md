* website: <https://arrizza.com/arduino-led-blink.html>
* installation: see <https://arrizza.com/setup-common.html>

## Summary

This is a simple Arduino app that blinks the on-board LED. It demonstrates basic setup, compilation and operation of the
Arduino.

## Configure cmake

See [Arduino Setup](https://arrizza.com/arduino-setup) for details on how to set up CMakeLists.txt for your arduino and
port.

## Setup

The on-board LED is pin 13 on the Arduino.

The setup function just sets that pin to be an OUTPUT.

```cpp
pinMode(13, OUTPUT);
```

## Main Loop

This blinks the LED. But, for extra flair, it blinks it quickly for about a second and then turns it off for a second.

It starts by turning on the LED and pausing for a short time. DELAYCOUNT is set to 50 and delay()
assumes milliseconds (ms), so the delay is 50ms.

```cpp
digitalWrite(13, HIGH);
delay(DELAYCOUNT);
```

It then goes into a for() loop for 10 cycles, Each cycle turns the LED off and then on again, delaying after each
operation. Since the delay is 50ms, this will blink the LED quickly off then on in 100ms. Ten cycles of 100ms is 1000
ms, so the loop runs for a total of one second.

```c++
for (int i = 0; i < 10; ++i) {
  digitalWrite(13, LOW);
  delay(DELAYCOUNT);
  
  digitalWrite(13, HIGH);
  delay(DELAYCOUNT);
}
```

Then the LED is turned off for a second.

```c++
digitalWrite(13, LOW);
delay(1000);
```

The whole loop() is repeated.

## Notes

* If the delay is too short, the LED looks like it is always on, just a bit dim. Try different values for DELAY. I found
  that a delay of 10-15ms, for me, caused the LED to look continuously on. A comfortable flash rate was 50 or 60 ms.
  Experiment what is good for you. There are some people (e.g. jet pilots) who can detect flashes at even faster blink
  rates.

* The maximum flash rate of the LED is very high. A Google search shows that some folks are trying 100KHz and higher
  rates. An interesting question is how fast could you blink an LED and have a detector reliably detect that flash rate?
